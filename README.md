Copyright 2014 John Turpish © 2014 John Turpish

This file is part of noor.
noor is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
noor is distributed in the hope that it will be amusing, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU Afferro General Public License along with noor (see COPYING).  If not, see <http://www.gnu.org/licenses/>.

 What the heck is noor?
========================
It's a programming language, and also the name of this implementation of it.
It's mostly intended to be humorous, but it is also supposed to be theoretically possible to do something
useful with it, so if you see an actual bug in the implementation feel free to share.

The idea comes from a common pattern in discussions of programming techniques. Someone will challenge
why anyone would ever use X, when a more general (typically lower-level) Y exists and can accomplish
anything X can (or could in combination with Z). Often there's some sort of cost associated with X, perhaps
runtime. One common counter argument is: sure you could just use the low-level Y and Z, but you could also
program using nothing but NAND gates.

If you get that skip this paragraph. The trick is that NAND, like NOR, forms a complete set for boolean
logic all by itself. Most people are comfortable with the fact that using some combination of AND, OR, and
NOT you can create any possible boolean function (I'm using the math meaning of function here, it has
to be single-valued, stateless, repeatable, predictable). So for a given number of true/false input variables
you can write any table you want to look up what the output should be and then implement that  table using
some combination of ANDs, ORs, and NOTs. And of course if you have multiple outputs you can just do it
again. Well, isn't it neat-o that NAND can do this trick all by itself? And so can NOR. If you're convinced
AND, OR, and NOT work but not NOR consider this:
* NOT(A)=NOR(A,A)
* OR(A,B)=NOT(NOR(A,B))
* AND(A,B)=NOR(NOT(A),NOT(B))

So just by hooking  up NORs you can accomplish most of the tasks of a computer program, in a very pure-functional way (getting stateful depends on the circumstances/environment).

This language is primarily an attempt to let someone do just that, but with NOR instead of NAND.

 Why NOR?
==========
**Why NAND‽**
* More zeros is better because they're more natural. How often do we represent true/1 as something (say a high voltage, a leading/falling edge, a change in elevation on a CD, etc.) and false/0 as the absence of that something? Prefer a lower energy state. Entropy, dude. It's the law! Let's not make those electrons and so on work harder than they need to.
* Unlike NAND, NOR Is also an English word with a closely related meaning. Specifically: NOR(A,B) is true if neither A 𝘯𝘰𝘳 B are true.
* I have poor hearing, and to my ears NOR sounds a lot like Noor, which is name of a pretty cool guy who has the same employer I do.

Let's check the score:

NAND | NOR | WHAT
-----|-----|------------------------------------------------
✓    |  ✓  | Forms a complete set of boolean logic by itself
X    |  ✓  | Has more zero outputs on a truth table
X    |  ✓  | An English word
X    |  ✓  | Kinda sounds like Noor

 Hooking up all these gates/functions is great and all, but then what?
=======================================================================
If we want to have the theoretical possibility of doing something interesting
with a program written in noor it needs to be able to interact with the
surrounding environment in some way. I wanted that mechanism to be in the
spirit of the concept, in particular it should:
* Be implemented fairly simply with only low-level stuff
* Be incredibly general, able to do just about anything one could want it to.
* Preferably be comically burdensome to use.

The obvious choice was syscall.

If you're unfamiliar with system calls you can check out a pretty good
Wikipedia article about it: http://en.wikipedia.org/wiki/System_call
But the basic idea is that it's one function that can do just about whatever
you need (at least when it comes to interacting with the OS). In fact many of
more familiar low-level functions are implemented in terms of a system call.
If you call write (as in man 2 write), that function is a convenience (and type
safety) wrapper for a syscall. Doing an IOCTL? Also a syscall(). The first
parameter is an integer (really an enum) whose value indicates what
functionality you're after. So for the above examples, write could be 1 while
ioctl might be 16. The rest of the parameters are kind of whatever (variadic),
and it's just up to you to check out the documentation and make sure you're
passing the right stuff in the right place.

 Is it Turing-complete?
========================
In the strict sense, no of course not. No machine with a real physical
existence is.
In the more common sense, I strongly believe so, but I'm more than willing to
listen if you want to convince me otherwise. I'm not great with math and I
don't have a CS degree or anything like that, so I haven't done any rigorous
proofs. If I were to try to prove it I would probably write a noor program that
follows the exact steps of a theoretical Turing machine. I'm thinking I'd use a
file as the tape, use lseek to move the tape back and forth, read and write to
observe and change the presence of a dot, with each byte in the file being a
possible dot position. I don't have the motivation or mental endurance to take
on something like that right now. Maybe we could delegate that to some
masochistic CS student - a SoC project along those lines would amuse me to no
end simply by existing.

 PORTABILITY
=============
This particular interpreter can be built on a wide variety of systems. Its only
dependencies are a reasonably up-to-date C++ compiler & std::, and also
boost::program_options. But if program_options is a challenge for you you could
modify the code to work without it in probably fifteen minutes or less
depending on how you choose to implement it. It's just argument parsing.

The noor code itself is remarkably non-portable. Your program will be specific
to things like sizeof(int), sizeof(size_t), sizeof(void*), and probably
byte order (but not necessarily consistently throughout all parts of the
program, 'cause that's fun).

It's also specific to the OS, due to the details of calling syscall. Your
program will have to know ahead of time not just the arguments, and
potentially the meaning of the return values, but also the exact values of the
first parameter - the syscall number. For example, "write" on Linux on x86_64
is 1, but on Linux on x86 it is 4. Just padding out your integers won't port
an x86 program because then you'd be trying to stat a file which probably
doesn't exist when you meant to output something.

 DETAILS
=========
A noor process is defined by an ordered list of bits. The index,
henceforth "id", is a signed integer. Bits with a negative id are read-only
data provided by the runtime. Non-negative bits are outputs from NOR functions
(sometimes I refer to them as gates, but they don't really behave as such),
whose inputs are defined statically in the noor source code.
The process iterates as follows:
1. Evaluate all NORs until a steady state or a cycle is detected (more later)
2. Disconnect or reconnect scratch bits to their NORs as appropriate
3. If bit id 0, the Do-or-Die bit, is 0 (die) terminate the program
4. If the DoD bit is 1 (do) perform a system call
5. Update the read-only bits
6. Repeat

The read-only bits are as follows:

* -1=The Clock Bit: 0 during the first iteration, 1 for the second, then 0,1,0,1
* -2 through -N-1: N is implementation-defined, generally sizeof(int)*8. These bits contain zeros initially, and the return value of the most recent syscall thereafter. The implementation should indicate the format (e.g. endianness)
* -N-2 through -M-N-1: M is implementation-definted, generally sizeof(void*)*8 . These bits contain the memory address of the start of the scratch buffer.
* -M-N-2 through -M-N-X-1: X is implementation-defined, sizeof(errno)*8 . These bits contain the value of errno OR contain the value errno had after most recent syscall.

The gate bits have specific meanings during steps 2 and 3:
0=The Do-or-Die bit: 0 indicates exit, 1 syscall preparedness
1 through L: L is implementation-defined, likely identical to N. These bits
    contain the syscall number, in a way the implementation documentation
    should make clear.
L+1 through L+K: K is implementation-defined, probably the size of a register,
    and on systems I use it is identical to M (anyone miss far pointers? Me neither).
    These bits contain the first argument to the specific system call being made.
L+iK+1 through L+(i+1)K : trying to get across that the previous specification
    was only the first argument, but syscall takes more than one argument and
    all follow immediately. How many there are depends on the implementation.
L+max(i)K+K+1 through L+max(i)K+K+Y : Disconnect bits.
    This is an integer which indicates how many of the leading scratch bits
    will be disconnected from their NORs (and left floating, unevaluating any
    Boolean logic) before the next syscall and left disconnected through the
    next bit evaluation step. If this number decreases bits get reconnected.
L+max(i)K+K+Y+1 through J: scratch bits. These are either the bits in the scratch
    buffer or the programmer may pretend they are, though the layout is in an
    implementation-defined fashion. They have no special meaning in and of
    themselves, but they are the only bits whose memory address can be
    reliably known, so they are very useful for system calls that require
    passing a pointer.
    J is a very, very large but implementation-defined number. It is likely
    memory-limited.

The source:
A noor source file has an even number of integers in it. They are paired
adjacently: the first and second are a pair, the second and third are a pair,
and so on. Each pair forms a "gate specification". The first integer is the bit
id of the first operand for the gate whose output is bit zero. The second
integer is the other operand for bit 0.
The encoding should be UTF-8.

Syntax:
Whitespace is any positive number of whitespace characters [space,tab,line feed,
carriage return,vertical tab,form feed]. Non-Latin-1 characters which are
whitespace (e.g. nbsp) may be recognized by an implementation, but they may
also not be.

An integer is an optional sign [+-] followed by some number of decimal
digits [0123456789]. It must not be preceeded by a non-whitespace character (if
it were it would be a comment). The implied value of the integer is exactly
what you think it is.

A comment is anything else. Specifically that means it starts with a
non-whitespace character that isn't part of an integer (it can't be a digit,
and may only be a sign if it's not followed by any digits). The comment
continues until whitespace is encountered.

Example:
#if 1
i=9, j= -1; //j is -1
return 0x4a;

That program is equivalent to:
1 -1 -1 0
which, coincidentally, is either going to exit or not, possibly with an error.
Not a great program.

 EVALUATION TERMINATION
========================
Step 1 of each iteration is evaluating the NORs. This should continue until
they stop flipping their bits (steady state). However, one may create a cycle
where a bit's value depends ultimately on its own value, so that it never stops
flipping. The example program from a few lines above does that (both bits
depend on each other and on the clock bit). The simplest program to do that
would simply be:
0 0
This is allowed by the language and not considered a syntactic error. The
implementation should terminate evaluation when it is discovered that the only
bits which may flip (should it continue) are in such a cycle. In other words,
it detected an infinite loop. At what exact point in that loop it terminates,
and thus what values these bits have, is completely up to the implementation.
It may be predictable, but it doesn't have to be. And that factoid means doing
this is not particularly useful and it is advised against.

 UNSPECIFIED BEHAVIOR
======================
It is a programmer error and leads to unspecified behavior if:
There are an odd number of integers in the source code.
One of the NORs specifies as input an ID that is less than any provided.
One of the NORs specifies as input an ID that is greater than any defined.
The address of the scratch bits is used without defining any scratch bits.
Not enough bits are specified to all the arguments to syscall clear.
    Though I strongly recommend implementers be reasonable about this.
There are no guarantees about atomic behavior or evaluation order. For example,
    bits from the syscall may be copied into the scratch area atomically or one
    bit at a time allowing Boolean evaluation in between or anywhere in between

Unspecified behavior means that whatever the implementation does is OK. It
signal an error, crash, exit gracefully, silently work, silently work in an
unexpected and surprising way, send your mom an email, or whatever combination
of these the implementer finds convenient.

 BEST PRACTICES
================
Yep. Because why not?
I recommend defining each gate on its own line, with no lines in-between.
Except at the start. You see, I like the bit ID to match up to the line number.
So I recommend doubling up some of the early bits two-per-line so that from
there on it's true. Most text editors start with line number 1, while the gates
begin at zero, so one double-gated line catches you up. Unless you include a
hashbang, then two lines double up, and so on.
One might choose to delay the doubling-up until one gets to bits that aren't
special with a need to add a specific comment.

For visual consistency it's usually better to put all the comments at the end
of the line.

Although comments may begin immediately following an integer, it is more clear
if there is a space there.

Don't create cycles in your NORs. It's just a bad idea.

 REACH THIS GUY
================
email: jbt@gmx.us
