/*
Copyright 2014 John Turpish © 2014 John Turpish

This file is part of noor.
noor is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
noor is distributed in the hope that it will be amusing, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU Afferro General Public License along with noor (see COPYING).  If not, see <http://www.gnu.org/licenses/>.
*/
#include <vector>

class Gate
{
    std::size_t depth_;
    bool value_;
    const Gate* op1_;
    const Gate* op2_;
    std::vector<Gate*> dependents_;
    bool connected_;
    void Notify( std::size_t depth );
public:
    Gate();
    bool Value() const;
    explicit operator bool()  const;
    void SetOperand( Gate* op, bool left );
    void Connect();
    void Disonnect();
    Gate& operator=(bool b);
    void Evaluate( std::size_t depth );
};
