/*
Copyright 2014 John Turpish © 2014 John Turpish

This file is part of noor.
noor is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
noor is distributed in the hope that it will be amusing, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU Afferro General Public License along with noor (see COPYING).  If not, see <http://www.gnu.org/licenses/>.
*/
#include "main.h"
#include "BitGroup.hh"
#include <boost/program_options.hpp>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <stdexcept>

int verbose = 0;
bool debug = false;

namespace po = boost::program_options;

long GetOp( std::istream& i, std::string& s );

std::ostream& operator<<( std::ostream& o, const Gate& g );

void Parse( std::ifstream& in, BitGroup& out );
void SystemCall( bool dryrun, int num, const std::array<SizedBitGroup<long>,5>& args, SizedBitGroup<int>& return_value );
void ImplementionHelp( std::ostream& o );

int main( int argc, const char* argv[] )
{
    try
    {
        po::options_description desc{ "Options" };
        desc.add_options()
            ( "help,h", "This help message." )
            ( "help-implementation,i", "Get the specific answers for all that implementation-defined stuff mentioned in the README." )
            ( "debug,d", "Enable debug output, good for debugging the noor program being evaluated." )
            ( "verbose,v", po::value<int>()->default_value(0), "Amount of verbose logging output for debugging the interpreter itself." )
            ( "dryrun,y", "Output what system calls would have been made, but don't make them." )
            ( "maxsyscalls,x", po::value<std::size_t>()->default_value(std::numeric_limits<std::size_t>::max()), "Terminate the program after this many system calls." )
            ( "execute,e", po::value<std::string>()->default_value("default.noor"), "Name of program file to be run." )
            ( "license,l", "Display licensing information and exit." )
            ;
        po::variables_map opts;
        po::positional_options_description pos;
        pos.add( "execute",  -1 );
        po::store( po::command_line_parser(argc, argv).options(desc).positional(pos).run(), opts );
        po::notify( opts );
        if ( opts.count("help") )
        {
            std::cout << desc << '\n';
            return 0;
        }
        if ( opts.count("license") )
        {
            std::cout <<
                "noor is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n"
                "noor is distributed in the hope that it will be amusing, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.\n"
                "You should have received a copy of the GNU Afferro General Public License along with noor (see COPYING).  If not, see <http://www.gnu.org/licenses/>.\n";
            return 0;
        }
        if ( opts.count("help-implementation") )
        {
            ImplementionHelp( std::cout );
            return 0;
        }
        debug = opts.count("debug");
        verbose = opts["verbose"].as<int>();
        SizedBitGroup<decltype(errno)> errno_bits;
        SizedBitGroup<long> address;
        SizedBitGroup<int>  syscall_return;
        SizedBitGroup<bool> clock_bit;
        SizedBitGroup<bool> do_or_die_bit;
        SizedBitGroup<int> syscall_number;
        std::array<SizedBitGroup<long>,5> syscall_args;
        SizedBitGroup<uint16_t> disconnect;
        DynamicBitGroup scratch_bits;
        errno_bits.chain(address)
                    .chain(syscall_return)
                    .chain(clock_bit)
                    .chain(do_or_die_bit)
                    .chain(syscall_number)
                    .chain(syscall_args[0]);
        for ( auto i = 1UL; i < syscall_args.size(); ++i )
        {
            syscall_args[i-1].chain(  syscall_args[i] );
        }
        syscall_args[4].chain( disconnect ).chain( scratch_bits );
        auto depth =  address.Size() +
                                 syscall_return.Size() +
                                 clock_bit.Size() +
                                 do_or_die_bit.Size() +
                                 syscall_number.Size() +
                                 syscall_args.size() * syscall_args[0].Size() +
                                 scratch_bits.Size();
        std::ifstream source{ opts["execute"].as<std::string>() };
        if ( source )
        {
            Parse( source, do_or_die_bit );
        }
        else
        {
            std::clog << "Trouble opening " << opts["execute"].as<std::string>() << '\n';
            return 2;
        }
        std::vector<uint8_t> scratch_buffer( scratch_bits.Size() / 8UL + 1 );
        address.FromInteger( reinterpret_cast<long>(scratch_buffer.data()) );
        address.Evaluate( depth );
        if ( debug || verbose )
            std::clog << "ScratchBuffer is @ " << static_cast<void*>( scratch_buffer.data() ) << '\n';
        for ( auto countdown = opts["maxsyscalls"].as<std::size_t>(); countdown; --countdown )
        {
            if ( debug )
            {
                std::clog << "ERRNO:    " << address << '\n';
                std::clog << "ADDRESS:  " << address << '\n';
                std::clog << "RETURNED: " << syscall_return << '\n';
                std::clog << "CLOCK BIT:" << clock_bit << '\n';
                std::clog << "DO OR DIE:" << do_or_die_bit << '\n';
                std::clog << "CALLNUM:  " << syscall_number << '\n';
                std::clog << "ARG0:     " << syscall_args[0] << '\n';
                std::clog << "ARG1:     " << syscall_args[1] << '\n';
                std::clog << "ARG2:     " << syscall_args[2] << '\n';
                std::clog << "ARG3:     " << syscall_args[3] << '\n';
                std::clog << "ARG4:     " << syscall_args[4] << '\n';
                std::clog << "DISCONNCT:" << disconnect << '\n';
                std::clog << "SCRATCH:  " << scratch_bits << '\n';
            }
            long i;
            for ( i = 0; i < disconnect.ToInteger() && i < scratch_bits.Size(); ++i )
            {
                scratch_bits[i].Disonnect();
            }
            for ( ; i < scratch_bits.Size(); ++i )
            {
                scratch_bits[i].Connect();
            }
            if ( do_or_die_bit[0] )
            {
                scratch_bits.Copy( scratch_buffer );
                if ( verbose > 2 )
                {
                    for ( auto rmb : scratch_buffer )
                    {
                        std::clog << std::hex<< static_cast<short>(rmb);
                    }
                    std::clog.put('\n');
                }
                if ( verbose > 4 )
                    std::clog << static_cast<void*>( scratch_buffer.data() ) << ',' << syscall_args[1].ToInteger() << ',' << reinterpret_cast<void*>(syscall_args[1].ToInteger()) << '\n';
                SystemCall( opts.count("dryrun"), syscall_number.ToInteger(), syscall_args, syscall_return );
                errno_bits.FromInteger( errno );
                if ( debug )
                    std::cerr << "errno (" << errno << "): " << strerror(errno) << '\n';
                scratch_bits.Assign( scratch_buffer );
            }
            else
            {
                return errno;
            }
            clock_bit[0] = ! clock_bit[0];
            clock_bit.Evaluate( depth );
        }
    }
    catch ( const std::exception& e )
    {
        std::clog << "ERROR: "  << e.what() << '\n';
        return 2;
    }
}

long GetOp( std::istream& i, std::string& s )
{
    char* end;
    do
    {
        i >> s;
        auto l  = strtol( s.c_str(), &end, 10 );
        if ( end > s.c_str() )
            return l;
    } while ( i );
    return 0;
}

void Parse(std::ifstream& in, BitGroup& out)
{
    std::string tok;
    for ( long idx = 0; in && idx >= 0; ++idx )
    {
        auto op1 = GetOp( in, tok );
        auto op2 = GetOp( in, tok );
        if ( in && debug )
        {
            std::clog << std::setw(3) << idx << "=NOR(" << op1 << ',' << op2 << ")\n";
        }
        out.At( idx )->SetOperand( out.At(op1), true );
        out.At( idx )->SetOperand( out.At(op2), false );
    }
}

void SystemCall( bool dryrun, int num, const std::array<SizedBitGroup<long>,5>& args, SizedBitGroup<int>& return_value )
{
    if ( verbose > 6 )
        std::clog << "SCRA?" << reinterpret_cast<void*>(args[1].ToInteger()) << '\n';
    if ( dryrun || debug )
    {
        std::clog << "syscall(" << num;
        for ( auto&& arg : args )
        {
            std::clog << ',' << std::hex << arg.ToInteger();
        }
        std::clog << ")=";
    }
    if ( dryrun )
    {
        std::clog << "? (0)\n";
    }
    else
    {
        auto ret = syscall( num, args[0].ToInteger(), reinterpret_cast<void*>(args[1].ToInteger()), args[2].ToInteger(), args[3].ToInteger(), args[4].ToInteger() );
        if ( debug )
            std::clog << ret << '\n';
        if ( verbose > 4 )
            std::clog << errno << " vs. " << EFAULT << '\n';
        return_value.FromInteger( ret );
    }
}

/**
 * This function borrowed with slight modification
 * @link http://stackoverflow.com/questions/1001307/detecting-endianness-programmatically-in-a-c-program
 * @author David Cournapeau
 */
bool is_big_endian()
{
    union {
        uint32_t i;
        char c[4];
    } bint = {0x01020304};
    return bint.c[0] == 1;
}

void ImplementionHelp( std::ostream& o )
{
    o <<
 "You are using an executable built for "
#ifdef __amd64__
 "amd64 a.k.a. x86_64"
#else
 "some architecture I haven't tested"
#endif
 ". The bit counts are as follows:"
    "\n\tN (count of bits in syscall return)=sizeof(int)*8=" << (sizeof(int)*8) <<
    "\n\tM (count of bits in scratch address)=sizeof(void*)*8=" << (sizeof(void*)*8) <<
    "\n\tL (count of bits in syscall number)=N"
    "\n\tK (count of bits in a syscall argument)=sizeof(long)*8=" << (sizeof(long)*8) <<
    "\n\tThere are five arguments to syscall, total."
    "\n\tJ, the highest possible bit ID, is limited by available in-process heap memory."
    "\n\tX, the size of errno, is " << (sizeof(errno)*8) <<
    "\n\tY, the count of disconnect bits, is 16"
 "\nThe bit/byte order is strictly big-endian for "
    "\n\t errno"
    "\n\t the scratch address"
    "\n\t syscall return"
    "\n\t syscall number"
    "\n\t syscall arguments"
    "\n\t disconnect"
 "\nregardless of the underlying architecture."
 "\nHowever, the scratch bits are laid out in native byte order, so that the"
    "\n\t first scratch bit is the most significant bit of the lowest-addressed"
    "\n\t byte, that address plus one refers to scratch bits 8-15, and so on."
 "\n So if you are on a little-endian system,";
 if ( is_big_endian() )
 {
     o << " which this \n particular executable is not built for,";
 }
 else
 {
     o << " which you are,";
 }
 o << "\n you'll need to be reorder the bytes if you pass the address of an "
    << "\n integer or a struct containing and integral type.\n";
}
