/*
Copyright 2014 John Turpish © 2014 John Turpish

This file is part of noor.
noor is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
noor is distributed in the hope that it will be amusing, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU Afferro General Public License along with noor (see COPYING).  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Gate.hh"
#include "main.h"
#include <algorithm>
#include <functional>
#include <iostream>

Gate::Gate()
:   depth_(sizeof(long)),
    value_(false),
    op1_(nullptr), op2_(nullptr),
    dependents_(),
    connected_(false)
{
}

void Gate::SetOperand( Gate* op, bool left )
{
    if ( left )
    {
        op1_ = op;
    }
    else
    {
        op2_ = op;
    }
    if ( !op )
        return;
    auto& d = op->dependents_;
    if ( std::find(d.begin(), d.end(), this) == d.end() )
        d.push_back( this );
    Connect();
}
bool Gate::Value() const
{
    return value_;
}
Gate::operator bool() const
{
    return value_;
}
Gate& Gate::operator=(bool b)
{
    if ( b != value_ )
    {
        value_ = b;
        Notify( depth_ );
    }
    return *this;
}
void Gate::Evaluate( std::size_t depth )
{
    depth_ = std::max( depth_, depth );
    if ( connected_ )
    {
        if ( value_ == (op1_->value_ || op2_->value_) )
        {
            value_ = !value_;
            if ( verbose )
            {
                std::clog << "NOR(" << op1_->value_ << ',' << op2_->value_ << ")=" << value_ << '@' << static_cast<void*>(this) << '\n';
            }
        }
        else
        {
            return;
        }
    }
    Notify( depth );
}
void Gate::Notify( std::size_t depth )
{
    std::for_each( dependents_.begin(), dependents_.end(),
                               std::bind(&Gate::Evaluate,std::placeholders::_1,depth-1) );
}
void Gate::Connect()
{
    connected_ = op1_ && op2_;
}
void Gate::Disonnect()
{
    connected_ = false;
}
