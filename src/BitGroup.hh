/*
Copyright 2014 John Turpish © 2014 John Turpish

This file is part of noor.
noor is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
noor is distributed in the hope that it will be amusing, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU Afferro General Public License along with noor (see COPYING).  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Gate.hh"
#include "main.h"
#include <array>
#include <functional>
#include <algorithm>
#include <list>
#include <iostream>

class BitGroup
{
protected:
    BitGroup* prev_ = nullptr;
    BitGroup* next_ = nullptr;
    virtual Gate* InnerAt( long idx ) = 0;
    virtual void ForEach( std::function<void(Gate&)> f ) = 0;
public:
    virtual ~BitGroup();
    Gate* At( long idx );
    Gate& operator[]( long idx );
    const Gate& operator[]( long idx ) const;
    virtual long Size() const = 0;
    BitGroup& chain( BitGroup& right );
    void Evaluate( std::size_t depth );
};

class DynamicBitGroup :  public BitGroup
{
    std::list<Gate> bits_;
    Gate* InnerAt( long idx );
    void ForEach( std::function<void(Gate&)> f );
public:
    long Size() const;
    void Copy( std::vector<uint8_t>& out ) const;
    void Assign( const std::vector<uint8_t>& in );
};
namespace impl
{
    template <typename IntegerType>
    struct BitCount
    {
        static const long value = sizeof(IntegerType) * 8UL;
    };
    template<>
    struct BitCount<bool>
    {
        static const long value = 1UL;
    };
}
template<typename IntegerType>
class SizedBitGroup : public BitGroup
{
    static const long N = impl::BitCount<IntegerType>::value;
    std::array<Gate,N> bits_;
    Gate* InnerAt( long idx )
    {
        return &( bits_.at(idx) );
    }
    void ForEach( std::function<void(Gate&)> f )
    {
        std::for_each( bits_.begin(), bits_.end(), f );
    }
public:
    long Size() const
    {
        return N;
    }
    void FromInteger( IntegerType i )
    {
        std::for_each( bits_.rbegin(), bits_.rend(), [&i](Gate&g){g=i&1;i>>=1;} );
        if ( verbose )
            std::clog << __PRETTY_FUNCTION__ <<  '(' << i << ")\n";
    }
    IntegerType ToInteger() const
    {
        IntegerType rv = 0;
        for ( auto&& b : bits_ )
        {
            rv <<= 1;
            rv |=  b.Value();
        }
        return rv;
    }
};
std::ostream& operator<<( std::ostream& o, const BitGroup& b );
