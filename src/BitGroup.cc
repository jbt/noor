/*
Copyright 2014 John Turpish © 2014 John Turpish

This file is part of noor.
noor is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
noor is distributed in the hope that it will be amusing, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU Afferro General Public License along with noor (see COPYING).  If not, see <http://www.gnu.org/licenses/>.
*/

#include "BitGroup.hh"
#include "main.h"

BitGroup::~BitGroup()
{
}
BitGroup& BitGroup::chain( BitGroup& right )
{
    next_ = &right;
    right.prev_ = this;
    return right;
}
Gate* BitGroup::At(long int idx)
{
    if ( verbose > 3 )
    {
        std::clog << __PRETTY_FUNCTION__ << '(' << idx
                        << ")w/size=" << Size()
                        << ";<-" << static_cast<void*>(prev_)
                        <<  ";->" << static_cast<void*>(next_)
                        << '\n';
    }
    if ( idx < 0 && prev_ )
        return prev_->At( idx + prev_->Size() );
    if ( idx >= Size() &&  next_ )
        return next_->At( idx - Size() );
    return InnerAt( idx );
}
Gate& BitGroup::operator[](long int idx)
{
    return *(At(idx));
}
const Gate& BitGroup::operator[](long int idx) const
{
    return *( const_cast<BitGroup&>(*this).At(idx) );
}
void BitGroup::Evaluate(std::size_t depth)
{
    if ( verbose )
        std::clog << "Evaluating " << Size() << " bits to a depth of " << depth << '\n';
    ForEach( std::bind(&Gate::Evaluate, std::placeholders::_1, depth) );
    if ( next_ )
        next_->Evaluate( depth );
}

Gate* DynamicBitGroup::InnerAt(long int idx)
{
    if ( idx >= Size() )
    {
        if ( verbose > 1 )
            std::clog << "Expanding DynamicBitGroup to " << (idx+1) << " bits.\n";
        bits_.resize( idx + 1 );
    }
    return &( *std::next(bits_.begin(), idx) );
}
long int DynamicBitGroup::Size() const
{
    return bits_.size();
}
void DynamicBitGroup::ForEach(std::function< void(Gate&) > f)
{
    std::for_each( bits_.begin(), bits_.end(), f );
}
void DynamicBitGroup::Assign(const std::vector< uint8_t >& in)
{
    auto it = bits_.begin();
    for ( auto b : in )
    {
        for ( auto mask = 0x80; mask; mask >>= 1, ++it )
        {
            if ( it == bits_.end() )
                return;
            *it = mask & b;
        }
    }//TODO verify
}
void DynamicBitGroup::Copy(std::vector< uint8_t >& out) const
{
    for ( long i = 0; i < Size(); ++i )
    {
        if ( i % 8 == 0 )
        {
            out.at( i / 8 ) = (*this)[i].Value() << 7;
        }
        else
        {
            out.at( i / 8 ) |= (*this)[i].Value() << ( 7 - i % 8 );
        }
    }//TODO verify
}

std::ostream& operator<<(std::ostream& o, const BitGroup& b)
{
    for ( auto i = 0L; i < b.Size(); ++i )
    {
        o << b[i].Value();
        if ( i % 4 == 3 )
            o << ' ';
    }
    o << '(' << std::dec << b.Size() << "bits)";
    return o;
}
